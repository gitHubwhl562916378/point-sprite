#version 330
uniform mat4 pMatrix,vMatrix,mMatrix;
uniform float uPointSize;
layout (location = 0) in vec3 aPosition;

void main(void)
{
    gl_Position = pMatrix * vMatrix * mMatrix * vec4(aPosition, 1);
    gl_PointSize = uPointSize;
}
