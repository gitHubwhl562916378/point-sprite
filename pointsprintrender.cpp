#include <random>
#include <chrono>
#include <QDebug>
#include "pointsprintrender.h"

void PointSprintRender::initsize(QOpenGLExtraFunctions *f, QImage &textureImg, int pointsCount)
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    texture_ = new QOpenGLTexture(textureImg);
    texture_->setWrapMode(QOpenGLTexture::DirectionS,QOpenGLTexture::ClampToEdge);
    texture_->setWrapMode(QOpenGLTexture::DirectionT,QOpenGLTexture::ClampToEdge);
    texture_->setMinMagFilters(QOpenGLTexture::Nearest,QOpenGLTexture::Linear);
    pointCount_ = pointsCount;
    QVector<GLfloat> vertVec;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(-1.0,1.0);
    for(int i = 0; i < pointCount_ * 3; i++){
        vertVec << dis(gen);
    }
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(vertVec.data(),vertVec.count() * sizeof(GLfloat));

    float pointsSizeRange[2]{0};
    f->glGetFloatv(GL_ALIASED_POINT_SIZE_RANGE,pointsSizeRange);
    minPointSize_ = pointsSizeRange[0];
    maxPointSize_ = pointsSizeRange[1];
    qDebug() << minPointSize_ << maxPointSize_;
}

void PointSprintRender::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix,float pointSize)
{
    f->glEnable(GL_CULL_FACE);
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_BLEND); //开启混合
    f->glEnable(GL_POINT_SPRITE); //开启点精灵模式，使用点块纹理
    f->glEnable(GL_PROGRAM_POINT_SIZE); //让顶点程序决定点块大小
    program_.bind();
    vbo_.bind();

    f->glActiveTexture(GL_TEXTURE0 + 0);
    program_.setUniformValue("sTexture",0);
    program_.setUniformValue("pMatrix",pMatrix);
    program_.setUniformValue("vMatrix",vMatrix);
    program_.setUniformValue("mMatrix",mMatrix);
    program_.setUniformValue("uPointSize",pointSize);
    program_.enableAttributeArray(0);

    texture_->bind(0);
    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3 * sizeof(GLfloat));
    f->glDrawArrays(GL_POINTS,0,pointCount_);

    program_.disableAttributeArray(0);
    texture_->release();
    vbo_.release();
    program_.release();
    f->glDisable(GL_CULL_FACE);
    f->glDisable(GL_DEPTH_TEST);
    f->glDisable(GL_BLEND);
    f->glDisable(GL_POINT_SPRITE);
    f->glDisable(GL_PROGRAM_POINT_SIZE);
}
