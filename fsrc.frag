#version 330
uniform sampler2D sTexture;
out vec4 fragColor;

void main(void)
{
    vec2 texCoor = gl_PointCoord;
    fragColor = texture2D(sTexture, texCoor);
}
