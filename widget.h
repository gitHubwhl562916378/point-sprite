#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "pointsprintrender.h"
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

private:
    PointSprintRender render_;
    QMatrix4x4 pMatrix_;
    QVector3D cameraLocation_;
};

#endif // WIDGET_H
