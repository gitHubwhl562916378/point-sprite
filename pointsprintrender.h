#ifndef POINTSPRINTRENDER_H
#define POINTSPRINTRENDER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

class PointSprintRender
{
public:
    PointSprintRender() = default;
    void initsize(QOpenGLExtraFunctions* f,QImage &textureImg,int pointsCount);
    void render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix, float pointSize);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    QOpenGLTexture *texture_{nullptr};
    int pointCount_ = 0;
    float maxPointSize_=0,minPointSize_=0;
};

#endif // POINTSPRINTRENDER_H
